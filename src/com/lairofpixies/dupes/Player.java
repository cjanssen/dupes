package com.lairofpixies.dupes;

public class Player {
	
	static int				playerCount		= 4;
	static Player[]			players			= new Player[4];
	static ColorScheme[]	playerColors	= new ColorScheme[4];
	
	ColorScheme				cs;
	/*
	 * color normal;
	 * color right;
	 * color wrong;
	 * color show;
	 * color press;
	 */
	boolean					active;
	int						score;
	int						squares;
	
	Player(ColorScheme cs) {
		this.cs = cs;
		reset();
	}
	
	public void reset() {
		// active = false;
		score = 0;
		// squares = 0;
	}
	
	public void inc() {
		score++;
	}
	
	static public void validatePlayers() {
		int countSoFar = 0;
		for (int i = 0; i < 4; i++) {
			if (countSoFar == playerCount)
				players[i].active = false;
			if (players[i].active)
				countSoFar++;
		}
		
		if (countSoFar < playerCount) {
			for (int i = 0; i < 4; i++) {
				if ((!players[i].active) && countSoFar < playerCount) {
					players[i].active = true;
					countSoFar++;
				}
			}
		}
	}
	
	static public void initPlayers() {
		// Player(color normal, color right, color wrong, color show, color press)
		FileManagement.loadColorSchemes();
		
		for (int p = 0; p < 4; p++)
			players[p] = new Player(playerColors[p]);
		players[0].active = true;
		Player.validatePlayers();
	}
	
	static public void assignColors() {
		for (int i = 0; i < 4; i++)
			players[i].active = Rect.initRects[i].selected;
	}
	
	static public void resetPlayers() {
		for (int i = 0; i < 4; i++)
			players[i].reset();
	}
	
}
