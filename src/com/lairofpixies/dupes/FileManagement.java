package com.lairofpixies.dupes;

import java.io.File;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class FileManagement {
	
	static public void loadFiles() {
		FileManagement.loadOptions();
	}
	
	static public void loadOptions() {
		String lines[];
		lines = dupes.main.loadStrings("options.txt");
		StringTokenizer st;
		String l = "";
		
		for (int i = 0; i < lines.length; i++) {
			l = lines[i];
			if (l == null || l.isEmpty() || l.startsWith("#"))
				continue;
			l = l.replaceAll("\\s", "");
			st = new StringTokenizer(l, ":");
			String option = st.nextToken();
			if (option.equals("winMode"))
				dupes.main.winMode = Integer.parseInt(st.nextToken());
			else if (option.equals("dupesToWin"))
				dupes.main.dupesToWin = Integer.parseInt(st.nextToken());
			else if (option.equals("gameplayLength"))
				dupes.main.gameplayLength = Integer.parseInt(st.nextToken()) * 1000; // s
			else if (option.equals("shrinkAmount"))
				Rect.options_shrinkAmount = Float.parseFloat(st.nextToken());
			else if (option.equals("shrinkTime"))
				Rect.blinkTime = Integer.parseInt(st.nextToken());
			else if (option.equals("sequenceStepTime"))
				SequenceAnimation.sequenceStepTime = Integer.parseInt(st.nextToken());
			else if (option.equals("sequenceStartTime"))
				SequenceAnimation.sequenceStartTime = Integer.parseInt(st.nextToken());
			else if (option.equals("repeatDelay"))
				SequenceAnimation.repeatDelay = Integer.parseInt(st.nextToken());
			else if (option.equals("fxOn"))
				Sound.fxOn = Boolean.valueOf(st.nextToken());
			else if (option.equals("musicOn"))
				Sound.musicOn = Boolean.valueOf(st.nextToken());
			else if (option.equals("fxVolume"))
				Sound.fxVolume = Float.parseFloat(st.nextToken());
			else if (option.equals("musicVolume"))
				Sound.musicVolume = Float.parseFloat(st.nextToken());
			else if (option.equals("background-Color"))
				dupes.main.bgColor = parseColor(st.nextToken());
		}
	}
	
	static public void loadColorSchemes() {
		
		String lines[];
		lines = dupes.main.loadStrings("colors.txt");
		StringTokenizer st;
		String l = "";
		// String name;
		ColorScheme cs;
		
		for (int i = 0; i < lines.length; i++) {
			l = lines[i];
			if (l == null || l.isEmpty() || l.startsWith("#"))
				continue;
			l = l.replaceAll("\\s", "");
			
			try {
				if (l.startsWith(">")) {
					l = l.substring(1);
					st = new StringTokenizer(l, "/");
					for (int j = 0; j < 4; j++)
						Player.playerColors[j] = dupes.main.colors.get(st.nextToken());
				} else {
					st = new StringTokenizer(l, "/");
					cs = new ColorScheme(st.nextToken());
					cs.normal = parseColor(st.nextToken());
					cs.right = parseColor(st.nextToken());
					cs.wrong = parseColor(st.nextToken());
					cs.show = parseColor(st.nextToken());
					cs.press = parseColor(st.nextToken());
					dupes.main.colors.put(cs.name, cs);
				}
			} catch (NoSuchElementException exc) {
				dupes.println(l + " not corret. will not be added");
			}
		}
	}
	
	static public int parseColor(String s) {
		StringTokenizer st = new StringTokenizer(s, ",");
		String sr = st.nextToken();
		String sb = st.nextToken();
		String sg = st.nextToken();
		return dupes.main.color(Integer.parseInt(sr), Integer.parseInt(sb), Integer.parseInt(sg));
	}
	
}
