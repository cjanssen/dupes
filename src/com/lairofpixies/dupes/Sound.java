package com.lairofpixies.dupes;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;

import android.util.Log;


public class Sound {
	
	// this is the audio player for short quick audio files
	// the asset manager helps us find specific files and can be used in the style of an array if needed
	// the audio manager controlls all the audio connected to it, enabeling overall volume and such
	
	static SoundPool	soundPool;
	static AssetManager	assetManager;
	static MediaPlayer	mediaPlayer 	= null;
	
	static int			nSounds			= 8;
	static int			soundIds[]		= new int[nSounds];
	static boolean[]	soundOn			= new boolean[nSounds];
	static int[]		soundTimer		= new int[nSounds];
	static String[]		soundsF			= { "p1w", "p1l", "p2w", "p2l", "p3w", "p3l", "p4w", "p4l" };
	
	static int			numberOfSongs	= 4;
	
	static boolean		fadeOutSong		= false, fadeInSong = false;
	static int			songTimer;
	
	static boolean		fxOn			= true, musicOn = true;										// will be overwritten by options
	static float		fxVolume		= 1f, musicVolume = 0.4f;										// will be overwritten by options
			
	// boolean switchToGameSong = true;
	static int			lastSong		= -1;
	
	static int			oneSound		= 2;
	
	public static void initSounds() {
		soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0); // (max #of streams, stream type, source quality) - see the android reference for details
		if (mediaPlayer == null)
			mediaPlayer = new MediaPlayer();
		assetManager = dupes.main.getAssets();
		try {
			for (int i = 0; i < nSounds; i++)
				soundIds[i] = soundPool.load(assetManager.openFd("samples/" + soundsF[i] + ".wav"), 0); // load the files
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void playMusic(boolean MainOrRandom) {
		if (!musicOn)
			return;
		loadSong(MainOrRandom);
		mediaPlayer.setVolume(0, 0);
		mediaPlayer.setLooping(true);
		mediaPlayer.start();
		fadeInSong = true;
		songTimer = dupes.millis;
		// println(musicVolume);
	}

	private static boolean musicPaused = false;
	public static boolean isPaused() { return musicPaused; }
	public static void pauseMusic() {
		if (mediaPlayer != null) {
			mediaPlayer.pause();
		}
		musicPaused = true;
	}

	public static void resumeMusic() {
		if (mediaPlayer != null && musicPaused) {
			mediaPlayer.start();
			initSounds();
		}
	}
	
	public static void playSuccess(int player) {
		if (!fxOn)
			return;
		int i = 0;
		if (oneSound != -1) {
			i = oneSound * 2;
		} else
			i = player * 2;
		soundPool.play(soundIds[i], fxVolume, fxVolume, 0, 0, 1);
		soundTimer[i] = dupes.millis;
	}
	
	public static void playFail(int player) {
		if (!fxOn)
			return;
		int i = 0;
		if (oneSound != -1) {
			i = oneSound * 2 + 1;
		} else
			i = player * 2 + 1;
		soundPool.play(soundIds[i], fxVolume, fxVolume, 0, 0, 1);
		soundTimer[i] = dupes.millis;
	}
	
	public static void updateSounds() {
		for (int i = 0; i < nSounds; i++) {
			if (soundOn[i])
				if (dupes.millis - soundTimer[i] < 1000) {
					float f = fxVolume * (1 - (dupes.millis - soundTimer[i]) / 1000f);
					soundPool.setVolume(soundIds[i], f, f);
				} else
					soundOn[i] = false;
		}
		
		if (fadeInSong) {
			if (dupes.millis - songTimer < dupes.fadeTime) {
				float f = musicVolume * ((float) (dupes.millis - songTimer) / dupes.fadeTime);
				mediaPlayer.setVolume(f, f);
				// System.out.println(f);
			} else
				fadeInSong = false;
		}
		
		if (fadeOutSong) {			
			if (dupes.millis - songTimer < dupes.fadeTime) {
				float f = musicVolume * (1 - (float) (dupes.millis - songTimer) / dupes.fadeTime);
				mediaPlayer.setVolume(f, f);
			} else {
				fadeOutSong = false;
				mediaPlayer.stop();
			}
		}
		
	}
	
	/**
	 * 
	 * true for main song
	 * 
	 * @param randomOrMain
	 */
	public static void loadSong(boolean MainOrRandom) {
		try {
			mediaPlayer.reset();
			AssetFileDescriptor afd;
			if (MainOrRandom) {
				afd = dupes.main.getAssets().openFd("samples/songs/dupes_main.ogg");
			} else {
				int song;
				do {
					song = (int) dupes.main.random(numberOfSongs);
				} while (song == Sound.lastSong);
				afd = dupes.main.getAssets().openFd("samples/songs/dupes_song_" + song + ".ogg");
				Sound.lastSong = song;
			}
			mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
			afd.close();
			mediaPlayer.prepare();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * make the song fade out
	 */
	public static void endSong() {
		fadeOutSong = true;
		songTimer = dupes.millis;
	}

	public static void stopMusic() {
		if (mediaPlayer != null)
			mediaPlayer.stop();
	}
	
}
