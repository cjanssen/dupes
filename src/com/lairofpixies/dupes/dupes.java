package com.lairofpixies.dupes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;
import android.view.MotionEvent;

// import android.util.Log;

public class dupes extends PApplet {
	
	// todo end when win
	// home button
	
	static dupes			main;
	
	Sequence[]				sequences			= new Sequence[4];
	SequenceAnimation[]		sequenceAnimations	= new SequenceAnimation[4];
	
	public static int		millis;
	int						lastMillis			= 0;		
	int						limitTime			= 0;
	static int				fadeTime			= 1500;
	int						waitTime			= 2000;
	int						startTime			= 0;
	boolean					started				= false;
	
	final int				TIME				= 0, DUPES = 1;
	int						winMode				= TIME;					// will be overwritten by options
	int						dupesToWin			= 10;						// will be overwritten by options
	int						gameplayLength		= 45000;					// will be overwritten by options
																			
	int						bgColor				= color(237, 237, 240);	// will be overwritten by options
																			
	ArrayList<Player>		winner				= new ArrayList<Player>();
	
	int						menuScreen			= 0;
	final int				PLAYER_SELECT		= 0, INSTRUCTIONS = 1, CREDITS = 2;
	// menuScreen: 0 - select player, 1 - select color, 2 - instructions, 3 - credits
	
	int						gameState			= -1;
	final int				INTRO				= -1, COLOR_SELECT = 0, GAME = 1, GAME_END = 2, WIN_SCREEN = 3;
	int						ready				= 0;
	PImage					titleImage, tapColor, squares, creditsTitle, creditsNames, howToTitle, howToText, win, tie;
	int						timeWarp 			= 0;
	int 					refMillis 			= -1;
	
	// for testing
	
	public static PVector[]	gridPoints;
	
	@Override
	public void setup() {
		main = this;
		orientation(LANDSCAPE);
		FileManagement.loadFiles();
		Player.initPlayers();
		// initBoard();
		initMenu();
		Sound.initSounds();
		otherInits();
		strokeWeight(8);
		lastMillis = millis();
		// SecretOptionScreen.setup();
		// Sound.playMusic(true);
	}
	
	public void resetColorSelection() {
		ready = 0;
		for (int i = 0; i < 4; i++)
			Rect.initRects[i].selected = false;
	}

	private boolean lockExit = false;

	@Override
	public void exit() {
		if (gameState == INTRO && menuScreen == PLAYER_SELECT && !lockExit) {
			super.exit();
		} else {
			setGameState(INTRO);
			menuScreen = PLAYER_SELECT;
			lockExit = true;
			Sound.stopMusic();
		}
	}

   @Override
   public void onBackPressed() {
   }
       
   @Override
   public void keyReleased() {
       if (key == CODED) {
           if (keyCode == android.view.KeyEvent.KEYCODE_BACK) {
   		       lockExit = false;
   		   }
   	   }
   }
   
   @Override 
   public void onPause() {
   		Sound.pauseMusic();
   		super.onPause();
   }

   @Override
   public void onResume() {
   		super.onResume();
   		Sound.resumeMusic();
   		lastMillis = millis();
   }


	public void initBoard() {
		calcGrid();
		ArrayList<Integer> g = new ArrayList<Integer>();
		
		int squaresPerPlayer = Grid.totalGridSz / Player.playerCount;
		int i;
		for (i = 0; i < 4; i++)
			Player.players[i].squares = squaresPerPlayer;
		
		for (int j = 1; j <= 4; j++) {
			if (Player.players[j - 1].active)
				for (i = 0; i < squaresPerPlayer; i++) {
					g.add(j);
				}
		}
		Collections.shuffle(g);
		
		int[] rectCount = new int[4];
		for (i = 0; i < 4; i++)
			rectCount[i] = 0;
		
		for (i = 0; i < Grid.totalGridSz; i++) {
			int playr = g.get(i);
			Rect.rects[i] = new Rect(gridPoints[i], playr, defaultSize, width / 130.0f, rectCount[playr - 1]);
			rectCount[playr - 1]++;
		}
	}
	
	public void createSequence(int player) {
		sequences[player] = new Sequence(3, Player.players[player].squares);
		sequenceAnimations[player] = new SequenceAnimation(sequences[player], player + 1);
		sequenceAnimations[player].enabled = Player.players[player].active;
	}
	
	public void initSequences() {
		for (int i = 0; i < 4; i++) {
			createSequence(i);
		}
	}
	
	Button[]	button	= new Button[5];
	
	public void initMenu() {
		button[0] = new Button(new PVector(width / 2 - min(width / 4, height / 4), height * 0.6f), 2, 0.7f, "2p.png");
		button[1] = new Button(new PVector(width / 2, height * 0.6f), 3, 0.7f, "3p.png");
		button[2] = new Button(new PVector(width / 2 + min(width / 4, height / 4), height * 0.6f), 4, 0.7f, "4p.png");
		button[3] = new Button(new PVector(width * 2 / 16f, height * 14 / 16f), 5, 0.6f, "how_to_play.png");
		button[4] = new Button(new PVector(width * 14 / 16f, height * 14 / 16f), 6, 0.6f, "credits.png");
		// button[5] = new Button(new PVector(width / 2, height * 14 / 16f), 7, 1f, "back.png");
		
		titleImage = loadImage("dupes_title.png");
		tapColor = loadImage("tap_your_colour.png");
		win = loadImage("win.png");
		tie = loadImage("tie.png");
		// instructions = loadImage("instruction_words.png");
		creditsTitle = loadImage("credits_title.png");
		creditsNames = loadImage("credits_text.png");
		squares = loadImage("squares.png");
		howToTitle = loadImage("how_to_play_title.png");
		howToText = loadImage("how_to_play_text.png");
		
		Rect.rectSz = (height - 60) / 2;
		for (int i = 0; i < 4; i++)
			Rect.initRects[i] = new Rect(new PVector(), i + 1, 1, width / 130.0f, 0);
		
		int xOff = 2 * width / 3 - Rect.rectSz - 10;
		int yOff = 20;
		int xOff2 = xOff + 20 + Rect.rectSz;
		int yOff2 = yOff + 20 + Rect.rectSz;
		Rect.initRects[0].pos.set(xOff, yOff, 0);
		Rect.initRects[1].pos.set(xOff2, yOff, 0);
		Rect.initRects[2].pos.set(xOff, yOff2, 0);
		Rect.initRects[3].pos.set(xOff2, yOff2, 0);
		
	}
	
	public void restartGame() {
		// 45 seconds
		timeWarp = 0;
		if (winMode == TIME) {
			limitTime = millis + gameplayLength + waitTime;
			startTime = millis + waitTime;
		}
		setGameState(GAME);
		Player.resetPlayers();
		Player.validatePlayers();
		initBoard();
		initSequences();
		started = false;
		Sound.playMusic(false);
	}
	
	public void otherInits() {
		endScreenA = new PVector(0, 0);
		endScreenB = new PVector(width, height);
		textFont(loadFont("dupe-SemiExpanded-48.vlw"));
		textAlign(CENTER, CENTER);
	}
	
	@Override
	public void draw() {
		millis += millis() - lastMillis;
		lastMillis = millis();
		/*
		 * if (limitTime == 0) {
		 * restartGame();
		 * }
		 */
		if (gameState == INTRO) {
			background(0,0,0);
			showMenuScreen();
		} else if (gameState == COLOR_SELECT) {
			background(0,0,0);
			imageMode(CORNER);
			int wi = tapColor.width;
			int hi = tapColor.height;
			float scale = min((height / 2f) / tapColor.height, (width / 3f) / (tapColor.width + 50));
			image(tapColor, 30, height / 2 - tapColor.height / 2f, scale * wi, scale * hi);
			Rect.rectSz = (height - 60) / 2;
			for (int i = 0; i < 4; i++)
				Rect.initRects[i].draw();
			// println("ready "+ready + " / wait for "+playerCount);
			if (ready == Player.playerCount) {
				Player.assignColors();
				restartGame();
			}
		} else if (gameState == GAME || gameState == GAME_END) {
			
			background(0,0,0);
			int i;
			
			if (gameState == GAME) {
				if (started) {
					for (i = 0; i < sequenceAnimations.length; i++)
						sequenceAnimations[i].update();
				} else if (millis >= startTime) {
					Rect.allRectsBlink();
					initSequences();
					started = true;
				}
				if (winMode == TIME && millis > limitTime)
					setGameState(GAME_END);
				else if (winMode == DUPES && countForWinner()) {
					setGameState(GAME_END);
					Sound.endSong();
					limitTime = millis;
				}
			}
			
			drawBoard();
			
			// overlay at start
			if (!started) {
				float fraction = min(1.0f, (startTime - waitTime / 2 - millis) / PApplet.parseFloat(waitTime / 2));
				if (fraction > 0) {
					fill(0, 0, 0, 255 * fraction);
					rect(-100, -100, width + 200, height + 200);
				}
			}
			
		}
		if (gameState == GAME_END) { // endgame
			// for some time, fade to black
			float fraction;
			// if (winMode== TIME)
			fraction = (millis - limitTime) / (float) fadeTime;
			// else
			fraction = (millis - limitTime) / (float) fadeTime;
			fill(0, 0, 0, 255.0f * fraction);
			rect(-100, -100, width + 200, height + 200);
			if (millis > limitTime + fadeTime)
				setGameState(WIN_SCREEN);
			Sound.updateSounds();
		}
		if (gameState == WIN_SCREEN) {
			drawWinner();
			Sound.updateSounds();
		}
		
		Sound.updateSounds();
	}
	
	public boolean countForWinner() {
		for (Player p : Player.players)
			if (p.score == dupesToWin)
				return true;
		return false;
	}
	
	public void drawBoard() {
		// rotate board 45 degrees
		pushMatrix();
		translate(width / 2, height / 2);
		rotate(-PI / 4.0f);
		translate(-width / 2, -height / 2);
		
		for (int i = 0; i < Grid.totalGridSz; i++) {
			Rect.rects[i].draw();
		}
		
		popMatrix();
	}
	
	public void showMenuScreen() {
		imageMode(CENTER);
		switch (menuScreen) {
			case PLAYER_SELECT: { // main menu
				int wi = titleImage.width;
				int hi = titleImage.height;
				float scale = (height / 3f) / titleImage.height;
				
				image(titleImage, width / 2, height / 2 - titleImage.height * scale * 5 / 8, scale * wi, scale * hi);
				image(squares, width / 2, height * 14 / 16f);
				for (int i = 0; i < 5; i++)
					button[i].draw();
				break;
			}
			case INSTRUCTIONS: { // instructions
				float howToTitleScale = getImageScale(howToTitle, width / 3f, height * 0.20f);
				float squaresScale = getImageScale(squares, width / 6f, height * 0.10f);
				float howToTextNamesScale = getImageScale(howToText, width / 3f, height * 0.50f);
				image(howToTitle, width / 2, height * 0.125f, howToTitle.width * howToTitleScale, howToTitle.height * howToTitleScale);
				image(squares, width / 2, height * 0.35f, squares.width * squaresScale, squares.height * squaresScale);
				image(howToText, width / 2, height * 0.725f, howToText.width * howToTextNamesScale, howToText.height * howToTextNamesScale);
				break;
			}
			case CREDITS: { // credits
				float creditsTitleScale = getImageScale(creditsTitle, width / 3f, height * 0.20f);
				float squaresScale = getImageScale(squares, width / 6f, height * 0.10f);
				float creditsNamesScale = getImageScale(creditsNames, width / 3f, height * 0.50f);
				image(creditsTitle, width / 2, height * 0.125f, creditsTitle.width * creditsTitleScale, creditsTitle.height * creditsTitleScale);
				image(squares, width / 2, height * 0.35f, squares.width * squaresScale, squares.height * squaresScale);
				image(creditsNames, width / 2, height * 0.725f, creditsNames.width * creditsNamesScale, creditsNames.height * creditsNamesScale);
				break;
			}
		}
	}
	
	public float getImageScale(PImage img, float maxWidth, float maxHeight) {
		float xScale = maxWidth / img.width;
		float yScale = maxHeight / img.height;
		if (xScale > yScale && xScale * img.height < maxHeight)
			return xScale;
		else
			return yScale;
	}
	
	public void managePressed(int mX, int mY) {
		int i;
		PVector mouse = new PVector(mX, mY);
		if (gameState == INTRO) {
			if (menuScreen == PLAYER_SELECT) {
				for (i = 0; i < 3; i++)
					if (button[i].pressed(mouse)) {
						Player.playerCount = i + 2;
						gameState = COLOR_SELECT;
					}
				if (button[3].pressed(mouse)) {
					menuScreen = INSTRUCTIONS;
				}
				if (button[4].pressed(mouse)) {
					menuScreen = CREDITS;
				}
			} else if (menuScreen == INSTRUCTIONS || menuScreen == CREDITS) {
				menuScreen = PLAYER_SELECT;
			}
		} else if (gameState == COLOR_SELECT) {
			for (int j = 0; j < 4; j++)
				if (Rect.initRects[j].pressed(mouse)) {
					Rect.initRects[j].selected = !Rect.initRects[j].selected;
					if (Rect.initRects[j].selected)
						Sound.playSuccess(j);
					else
						Sound.playFail(j);
					if (Rect.initRects[j].selected)
						ready++;
					else
						ready--;
				}
		}
		
		if (gameState == GAME) {
			
			// compensate board rotation 45 degrees
			PVector offs = new PVector(width / 2, height / 2);
			mouse.sub(offs);
			mouse.rotate(PI / 4.0f);
			mouse.add(offs);
			
			for (i = 0; i < Grid.totalGridSz; i++)
				if (Rect.rects[i].pressed(mouse)) {
					process(Rect.rects[i]);
					return;
				}
		} else if (gameState == WIN_SCREEN && millis > limitTime + 2 * fadeTime) {
			setGameState(INTRO);
		}
	}
	
	public void process(Rect rect) {
		int pl = rect.player - 1;
		// play(rect.player,rect.id);
		// println("S"+ player+"-"+note(tone)+".mp3");
		// println(rect.player+","+rect.id);
		if (sequenceAnimations[pl].playing())
			return;
		
		Sequence seq = sequences[pl];
		sequenceAnimations[pl].delayRepeat();
		if (seq.validKey(rect.id)) {
			rect.showTouched();
			if (seq.completed()) {
				// allRectsSuccess(rect.player);
				rect.showSuccess();
				Sound.playSuccess(pl);
				Player.players[pl].score++;
				createSequence(pl);
			}
		} else {
			// rect.showFail();
			Sound.playFail(pl);
			Rect.allRectsFail(rect.player);
			createSequence(pl);
		}
	}
	
	public void computeWinner() {
		winner.clear();
		int winnercount = -1;
		for (int i = 0; i < 4; i++)
			if (Player.players[i].active && Player.players[i].score >= winnercount) {
				if (Player.players[i].score > winnercount) {
					winnercount = Player.players[i].score;
					winner.clear();
				}
				winner.add(Player.players[i]);
			}
	}
	
	public void setGameState(int newState) {
		gameState = newState;
		if (newState == GAME_END) {
			computeWinner();
		} else if (newState == INTRO) {
			resetColorSelection();
		}
	}
	
	@Override
	public void onStop() {
		if (!Sound.isPaused()) {
			if (Sound.soundPool != null) { // must be checked because or else crash when return from landscape mode
				Sound.soundPool.release(); // release the player
			}
			Sound.pauseMusic();
		}
		super.onStop(); // call onDestroy on super class
	}
	
	@Override
	public void onDestroy() {
		if (Sound.soundPool != null) { // must be checked because or else crash when return from landscape mode
			Sound.soundPool.release(); // release the player
		}
		Sound.stopMusic();
		super.onDestroy(); // call onDestroy on super class
	}
	
	public int tileSize() {
		int w = width;
		int h = height;
		while (h != 0) {
			if (w > h) {
				w = w - h;
			} else {
				h = h - w;
			}
		}
		while (w > 100)
			w /= 2;
		return w;
	}
	
	// winner color area. defined in dupe.otherInits
	PVector	endScreenA, endScreenB;
	
	// draws winscreen
	public void drawWinner() {
		// set background to winner colors
		int w = (int) ((endScreenB.x - endScreenA.x) / winner.size());
		int h = (int) (endScreenB.y - endScreenA.y);
		
		// draw win/draw text
		if (winner.size() == 1) {
			fill(winner.get(0).cs.normal);
			noStroke();
			rect(endScreenA.x, endScreenA.y, w, h);
			
			int wi = win.width;
			int hi= win.height;
			float scale = (h/3.5f)/ win.height;
			imageMode(CENTER);
			image(win, endScreenA.x+w/2, endScreenA.y+h/2, wi*scale, hi*scale);

		} else {
			for (int i=0; i < winner.size();i++) {
				fill(winner.get(i).cs.normal);
				noStroke();
		  // stroke(winner.get(i).normal);
				rect(endScreenA.x+w*i, endScreenA.y, w, h);
			}

			int wi = tie.width;
			int hi= tie.height;
			float scale = (h/3.5f)/ tie.height;
			imageMode(CENTER);
			image(tie, (endScreenA.x+endScreenB.x)/2, endScreenA.y+h/2, wi*scale, hi*scale);
		}

		// fade in
		float fraction = (millis - limitTime - fadeTime) / (float) fadeTime;
		fraction = min(1, fraction);
		fill(0, 0, 0, 255 * (1 - fraction));
		rect(endScreenA.x, endScreenA.y, endScreenB.x - endScreenA.x, h);
	}
	
	HashMap<String, ColorScheme>	colors		= new HashMap<String, ColorScheme>();
	
	float							defaultSize	= 0.95f;
	int								selectTimeT	= 1000;
	
	int								gridCount	= 10;
	
	public void calcGrid() {
		// choose a random board depending on players
		boolean success = false;
		while (!success) {
			Grid g = new Grid((int) random(gridCount));
			success = g.acceptsPlayers(Player.playerCount);
			if (success)
				g.constructGrid();
		}
	}
	
	@Override
	public boolean surfaceTouchEvent(android.view.MotionEvent me) {
		try {
			int action = (me.getAction() & MotionEvent.ACTION_MASK);
			
			if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {
				
				final int pointerIndex = (me.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				final int pointerId = me.getPointerId(pointerIndex);
				
				managePressed((int) me.getX(pointerId), (int) me.getY(pointerId));
			}
		} catch (Exception exc) {
		}
		return super.surfaceTouchEvent(me);
	}
	
	@Override
	public int sketchWidth() {
		return displayWidth;
	}
	
	@Override
	public int sketchHeight() {
		return displayHeight;
	}
}
