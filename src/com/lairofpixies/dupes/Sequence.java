package com.lairofpixies.dupes;

import java.util.ArrayList;

import processing.core.PApplet;

public class Sequence {
	ArrayList<Integer>	seq	= new ArrayList<Integer>();
	int					index;
	boolean				failed;
	
	Sequence(int len, int keycount) {
		index = 0;
		failed = false;
		for (int i = 0; i < len; i++)
			seq.add(PApplet.parseInt(dupes.main.random(keycount)));
	}
	
	public int length() {
		return seq.size();
	}
	
	// when user presses call this
	public boolean validKey(int newKey) {
		if (failed || index >= seq.size())
			return false;
		boolean success = (newKey == seq.get(index));
		if (success)
			index = index + 1;
		else
			failed = true;
		
		return success;
	}
	
	// what is the user score
	public int score() {
		return index;
	}
	
	// if the sequence was successfully completed
	public boolean completed() {
		return (index == length()) && !failed;
	}
	
	// for showing it
	public int getKey(int ndx) {
		return seq.get(ndx);
	}
}