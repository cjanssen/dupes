package com.lairofpixies.dupes;

import processing.core.PImage;
import processing.core.PVector;

public class Button {
	
	PVector	pos;
	int		number;
	float	size;
	PImage	img;
	
	Button(PVector pos, int number, float size, String img) {
		this.pos = pos;
		this.number = number;
		this.img = dupes.main.loadImage(img);
		this.size = (dupes.main.height / 8f) / this.img.height * size;
	}
	
	public float x() {
		return pos.x; // + img.width*0.5*size;
	}
	
	public float y() {
		return pos.y; // + img.height*0.5*size;
	}
	
	public float width() {
		return img.width * size;
	}
	
	public float height() {
		return img.height * size;
	}
	
	public void draw() {
		dupes.main.image(img, x(), y(), width(), height());
	}
	
	public boolean pressed(PVector p) {
		float tx = x();
		float ty = y();
		return p.x >= tx - width() / 2 && p.x <= tx + width() / 2 && p.y >= ty - dupes.main.height / 2 && p.y <= ty + height() / 2;
	}
}
