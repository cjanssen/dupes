package com.lairofpixies.dupes;

public class ColorScheme {
	
	String	name;
	
	int		normal;
	int		right;
	int		wrong;
	int		show;
	int		press;
	
	ColorScheme(String n) {
		name = n;
	}
	
	ColorScheme(String name, int normal, int right, int wrong, int show, int press) {
		this.name = name;
		this.normal = normal;
		this.right = right;
		this.wrong = wrong;
		this.show = show;
		this.press = press;
	}
}
