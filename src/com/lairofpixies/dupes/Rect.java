package com.lairofpixies.dupes;

import processing.core.PVector;

public class Rect {
	
	static Rect[]	rects;
	static Rect[]	initRects				= new Rect[4];
	
	static int		rectSz;
	static int		blinkTime				= 300;			// will be overwritten by options
	static boolean	options_samecolorShow	= true;
	static boolean	options_shrink			= true;
	static float	options_shrinkAmount	= 0.5f;		// will be overwritten by options
	static boolean	options_exclusivePulses	= true;
	
	PVector			pos;
	int				player;
	int				clr;
	int				time;
	float			size;
	float			originalSize;
	float			radius;
	
	int				state;
	// 0: normal, 1: pressed, 2: show, 3: success, 4: fail
	
	int				id;
	
	boolean			selected;
	
	Rect(PVector pos, int player, float size, float radius, int id) {
		this.pos = pos;
		this.player = player;
		this.originalSize = size;
		this.size = size;
		this.id = id;
		this.state = 0;
		this.radius = radius;
	}
	
	public float x() {
		return pos.x + rectSz * 0.5f * (1 - size);
	}
	
	public float y() {
		return pos.y + rectSz * 0.5f * (1 - size);
	}
	
	public float width() {
		return rectSz * size;
	}
	
	public float height() {
		return rectSz * size;
	}
	
	public void resetColors() {
		this.state = 0;
		this.time = 0;
	}
	
	public void updateColors() {
		if (dupes.millis > time)
			state = 0;
		
		if (state == 0) {
			clr = Player.players[player - 1].cs.normal;
			size = originalSize;
		}
		if (state == 1)
			clr = Player.players[player - 1].cs.press;
		if (state == 2) {
			if (options_samecolorShow) {
				clr = Player.players[player - 1].cs.normal;
			} else {
				clr = Player.players[player - 1].cs.show;
			}
			if (options_shrink) {
				// size = (1.0- pow((time-millis)/(float)blinkTime, 0.5) * options_shrinkAmount) * originalSize;
				size = (1f - dupes.abs(dupes.sin((time - dupes.millis) * 0.5f / (float) blinkTime * dupes.PI)) * options_shrinkAmount) * originalSize;
			} else {
				size = originalSize;
			}
		}
		if (state == 3) {
			boolean blinkState = ((dupes.millis - time + 2 * blinkTime) % (blinkTime / 2)) < blinkTime / 4;
			if (blinkState)
				clr = Player.players[player - 1].cs.right;
			else
				clr = Player.players[player - 1].cs.normal;
		}
		if (state == 4)
			clr = Player.players[player - 1].cs.wrong;
		if (state == 5) {
			boolean blinkState = ((dupes.millis - time + 2 * blinkTime) % (blinkTime / 2)) < blinkTime / 4;
			if (blinkState)
				clr = Player.players[player - 1].cs.press;
			else
				clr = Player.players[player - 1].cs.normal;
		}
	}
	
	public void draw() {
		if (dupes.main.gameState == 0) {
			if (selected)
				clr = Player.players[player - 1].cs.normal;
			else
				clr = Player.players[player - 1].cs.press;
		} else
			updateColors();
		dupes.main.fill(clr);
		dupes.main.noStroke();
		dupes.main.rect(x(), y(), width(), height(), radius);
	}
	
	public boolean pressed(PVector p) {
		float tx = x();
		float ty = y();
		return p.x >= tx && p.x <= tx + width() && p.y >= ty && p.y <= ty + height();
	}
	
	public void resetTimer() {
		this.time = dupes.millis + blinkTime;
	}
	
	public void showTouched() {
		state = 1;
		resetTimer();
	}
	
	public void showSuccess() {
		state = 3;
		resetTimer();
		time = time + blinkTime;
	}

	public void showReady() {
		state = 5;
		resetTimer();
		time = time + blinkTime;
	}
	
	public void showFail() {
		state = 4;
		resetTimer();
	}
	
	public void showShow() {
		state = 2;
		resetTimer();
	}
	
	static public void allRectsBlink() {
		for (int i = 0; i < rects.length; i++)
			rects[i].showReady();
	}
	
	static public void allRectsSuccess(int player) {
		for (int i = 0; i < rects.length; i++)
			if (rects[i].player == player)
				rects[i].showSuccess();
	}
	
	static public void allRectsFail(int player) {
		for (int i = 0; i < rects.length; i++)
			if (rects[i].player == player)
				rects[i].showFail();
	}
}
