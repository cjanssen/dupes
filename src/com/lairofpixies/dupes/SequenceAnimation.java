package com.lairofpixies.dupes;

public class SequenceAnimation {
	
	static int	sequenceStepTime	= 500;	// will be overwritten by options
	static int	sequenceStartTime	= 1200; // will be overwritten by options
	static int	repeatDelay			= 3000; // will be overwritten by options
	static int	nextAvailableSlot	= 0;
	
	Sequence	seq;
	int			owner;
	int			index;
	int			switchTime;
	int			timer;
	int			repeatTime;
	boolean		done;
	boolean		enabled;
	
	SequenceAnimation(Sequence seq, int player) {
		this.seq = seq;
		this.owner = player;
		this.enabled = true;
		restart();
		this.switchTime = dupes.millis + sequenceStartTime;
	}
	
	public void restart() {
		this.index = 0;
		this.switchTime = dupes.millis;
		this.done = false;
		delayRepeat();
	}
	
	public void delayRepeat() {
		repeatTime = dupes.millis + repeatDelay;
	}
	
	// returns true when anim finished
	public boolean update() {
		if (!enabled)
			return false;
		if (done) {
			if (dupes.millis >= repeatTime) {
				restart();
				done = false;
			}
		} else {
			boolean blink = false;
			
			if (Rect.options_exclusivePulses) {
				if (dupes.millis >= switchTime && dupes.millis >= nextAvailableSlot) {
					blink = true;
					switchTime = dupes.millis + sequenceStepTime;
					nextAvailableSlot = dupes.millis + sequenceStepTime / 4;
				}
			} else { // not exclusive pulses (original)
				if (dupes.millis >= switchTime) {
					blink = true;
					switchTime += sequenceStepTime;
				}
			}
			
			if (blink) {
				int rectI = seq.getKey(index);
				Rect.rects[rectIndex(owner, rectI)].showShow();
				index++;
				if (index == seq.length()) {
					delayRepeat();
					done = true;
				}
			}
		}
		return done;
	}
	
	public boolean playing() {
		return !done;
	}
	
	public int rectIndex(int player, int id) {
		for (int i = 0; i < Rect.rects.length; i++) {
			if (Rect.rects[i].player == player && Rect.rects[i].id == id)
				return i;
		}
		// error!
		return -1;
	}
}
