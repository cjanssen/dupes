package com.lairofpixies.dupes;

import processing.core.PVector;

public class Grid {
	
	static int	totalGridSz;
	
	int[][]		cells;
	boolean		extraGrid;
	int[][][]	displacedCells;
	float		hDiagSz, vDiagSz;
	boolean		done;
	
	Grid(int number) {
		done = false;
		extraGrid = false;
		switch (number) {
			case 0:
				diamondGrid();
				break;
			case 1:
				diamondFullGrid();
				break;
			case 2:
				diamondHoleGrid();
				break;
			case 3:
				diamondMirrorGrid();
				break;
			case 4:
				diamondMirrorConnectedGrid();
				break;
			case 5:
				diamondAnglesGrid();
				break;
			case 6:
				diamondLinesGrid();
				break;
			case 7:
				diamondLargeGrid();
				break;
			case 8:
				diamondMassiveGrid();
				break;
			case 9:
				diamondDisplacedGrid();
				break;
		}
		calcGridSize();
		done = true;
	}
	
	public boolean acceptsPlayers(int nplayers) {
		if (!done)
			return false;
		if (totalGridSz % nplayers == 0)
			return true;
		
		return false;
	}
	
	public void calcGridSize() {
		int gridW = cells.length;
		int gridH = cells[0].length;
		totalGridSz = 0;
		
		for (int x = 0; x < gridW; x++) {
			for (int y = 0; y < gridH; y++) {
				if (cells[x][y] == 1)
					totalGridSz++;
			}
		}
		
		if (extraGrid) {
			for (int k = 0; k < displacedCells.length; k++) {
				for (int x = 0; x < displacedCells[k].length; x++) {
					for (int y = 0; y < displacedCells[k][0].length; y++) {
						if (displacedCells[k][x][y] == 1)
							totalGridSz++;
					}
				}
			}
		}
	}
	
	public void constructGrid() {
		if (!done)
			return;
		int x, y;
		int gridW = cells.length;
		int gridH = cells[0].length;
		dupes.gridPoints = new PVector[totalGridSz];
		Rect.rects = new Rect[totalGridSz];
		Rect.rectSz = (int) dupes.min(dupes.main.width / (hDiagSz * dupes.sqrt(2.0f)), dupes.main.height / (vDiagSz * dupes.sqrt(2.0f)));
		int xoff = (int) (dupes.main.width * 0.5f - gridW * Rect.rectSz * 0.5f);
		int yoff = (int) (dupes.main.height * 0.5f - gridH * Rect.rectSz * 0.5f);
		int cellCount = 0;
		for (x = 0; x < gridW; x++) {
			for (y = 0; y < gridH; y++) {
				if (cells[x][y] == 1) {
					dupes.gridPoints[cellCount] = new PVector(xoff + x * Rect.rectSz, yoff + y * Rect.rectSz);
					cellCount++;
				}
			}
		}
		
		if (extraGrid) {
			// displaced cells
			for (int k = 0; k < displacedCells.length; k++) {
				gridW = displacedCells[k].length;
				gridH = displacedCells[k][0].length;
				xoff = (int) (dupes.main.width * 0.5f - gridW * Rect.rectSz * 0.5f);
				yoff = (int) (dupes.main.height * 0.5f - gridH * Rect.rectSz * 0.5f);
				for (x = 0; x < gridW; x++) {
					for (y = 0; y < gridH; y++) {
						if (displacedCells[k][x][y] == 1) {
							dupes.gridPoints[cellCount] = new PVector(xoff + x * Rect.rectSz, yoff + y * Rect.rectSz);
							cellCount++;
						}
					}
				}
			}
		}
	}
	
	// descriptions
	public void diamondGrid() {
		cells = new int[][] { { 0, 1, 0, 0, 0 }, { 1, 1, 1, 1, 0 }, { 0, 1, 0, 1, 0 }, { 0, 1, 1, 1, 1 }, { 0, 0, 0, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondFullGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 1, 1, 1, 0 }, { 1, 1, 0, 1, 1 }, { 0, 1, 1, 1, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondHoleGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 1, 0, 1, 0 }, { 1, 0, 0, 0, 1 }, { 0, 1, 0, 1, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondMirrorGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 1, 1, 0, 0 }, { 1, 1, 0, 1, 1 }, { 0, 0, 1, 1, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondMirrorConnectedGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 1, 1, 1, 0 }, { 1, 1, 0, 1, 1 }, { 0, 1, 1, 1, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondAnglesGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 0, 1, 0, 0 }, { 1, 1, 0, 1, 1 }, { 0, 0, 1, 0, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondLinesGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 0, 1, 1, 0 }, { 1, 1, 0, 1, 1 }, { 0, 1, 1, 0, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4;
		vDiagSz = 3;
	}
	
	public void diamondLargeGrid() {
		cells = new int[][] { { 0, 0, 1, 0, 0, 0, 0 }, { 0, 1, 1, 1, 0, 0, 0 }, { 1, 1, 1, 1, 1, 0, 0 }, { 0, 1, 1, 0, 1, 1, 0 }, { 0, 0, 1, 1, 1, 1, 1 }, { 0, 0, 0, 1, 1, 1, 0 },
				{ 0, 0, 0, 0, 1, 0, 0 } };
		hDiagSz = 5;
		vDiagSz = 3;
	}
	
	public void diamondMassiveGrid() {
		cells = new int[][] { { 0, 0, 0, 1, 0, 0, 0, 0 }, { 0, 0, 1, 1, 1, 0, 0, 0 }, { 0, 1, 0, 1, 0, 1, 0, 0 }, { 1, 1, 1, 0, 1, 0, 1, 0 }, { 0, 1, 0, 1, 0, 1, 1, 1 },
				{ 0, 0, 1, 0, 1, 0, 1, 0 }, { 0, 0, 0, 1, 1, 1, 0, 0 }, { 0, 0, 0, 0, 1, 0, 0, 0 } };
		hDiagSz = 5;
		vDiagSz = 4;
	}
	
	public void diamondDisplacedGrid() {
		cells = new int[][] { { 0, 1, 1, 0, 0 }, { 1, 1, 1, 1, 0 }, { 1, 1, 0, 1, 1 }, { 0, 1, 1, 1, 1 }, { 0, 0, 1, 1, 0 } };
		hDiagSz = 4.5f;
		vDiagSz = 3.5f;
		
		extraGrid = true;
		displacedCells = new int[][][] {
				{ { 0, 0, 1, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 1, 0, 0 } },
				{ { 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0 }, { 1, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 1 }, { 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0 } } };
	}
	
}